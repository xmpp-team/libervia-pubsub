-- we check version of the database before doing anything
-- and stop execution if not good
\set ON_ERROR_STOP
DO $$
DECLARE ver text;
BEGIN
    SELECT value INTO ver FROM metadata WHERE key='version';
    IF NOT FOUND OR ver!='9' THEN
        RAISE EXCEPTION 'This update file needs to be applied on database schema version 9, you use version %',ver;
    END IF;
END$$;
\unset ON_ERROR_STOP
-- end of version check

/* subscriptions table updates */
/* to handle external nodes */
ALTER TABLE subscriptions ALTER COLUMN node_id DROP NOT NULL;
ALTER TABLE subscriptions ADD COLUMN ext_service text;
ALTER TABLE subscriptions ADD COLUMN ext_node text;
/* and to mark a subscription as public */
ALTER TABLE subscriptions ADD COLUMN public boolean NOT NULL DEFAULT FALSE;
ALTER TABLE subscriptions ADD UNIQUE (entity_id, ext_service, ext_node);

UPDATE metadata SET value='10' WHERE key='version';
